﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Mindgaze.Utilities.Extensions
{
    public static class AssemblyResourceLoaderExtensions
    {
        public static Stream LoadResource(this Assembly assembly, string resourceName, string resourcePrefix = "Compiler.Preprocess.Resources")
        {
            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }

            if (String.IsNullOrEmpty(resourceName) || String.IsNullOrWhiteSpace(resourceName))
            {
                throw new ArgumentException($"Please supply valid {resourceName}");
            }

            if (String.IsNullOrEmpty(resourcePrefix) || String.IsNullOrWhiteSpace(resourcePrefix))
            {
                throw new ArgumentException($"Please supply valid {resourcePrefix}");
            }

            return assembly.GetManifestResourceStream($"{assembly.GetName().Name}.{resourcePrefix}.{resourceName}");
        }
    }

}
