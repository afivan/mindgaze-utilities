﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mindgaze.Utilities.Helpers
{
    public static class PathHelper
    {
        public static string GetDirectoryName(String filePath)
        {
            if (String.IsNullOrEmpty(filePath) || String.IsNullOrWhiteSpace(filePath))
            {
                return String.Empty;
            }

            if (filePath.EndsWith("\\") || filePath.EndsWith("/"))
            {
                // This is most probably a folder
                return filePath;
            }

            var indexOfBackslash = filePath.LastIndexOf('\\');
            var indexOfSlash = filePath.LastIndexOf('/');

            if ((indexOfBackslash == indexOfSlash) && indexOfSlash < 0)
            {
                // No / or \ found, return current directory
                return ".\\";
            }

            var greaterLastIndex = indexOfBackslash > indexOfSlash ? indexOfBackslash : indexOfSlash;

            return filePath.Substring(0, greaterLastIndex);
        }

        public static string CombinePathRelativeToFile(String filePath, String pathToLoad)
        {
            if (pathToLoad.StartsWith("/") || pathToLoad.StartsWith("\\"))
            {
                // This is an absolute path relative to the current folder
                return pathToLoad.Substring(1);
            }

            return $"{GetDirectoryName(filePath)}{System.IO.Path.DirectorySeparatorChar}{pathToLoad}";
        }
    }
}
