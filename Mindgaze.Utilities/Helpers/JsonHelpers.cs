﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Mindgaze.Utilities.Helpers
{
    public static class JsonHelpers
    {
        public static async Task<JObject> ReadJObjectAsync(String path)
        {
            JObject jObject = null;

            await Task.Factory.StartNew(() =>
            {

                using (var jsonReader = new JsonTextReader(new StreamReader(File.OpenRead(path))))
                {
                    jObject = JObject.Load(jsonReader);
                }
            });

            return jObject;
        }

        public static async Task<JSchema> ReadJSchemaAsync(String path)
        {
            JSchema jSchema = null;

            await Task.Factory.StartNew(() =>
            {
                using (var jsonReader = new JsonTextReader(new StreamReader(File.OpenRead(path))))
                {
                    jSchema = JSchema.Load(jsonReader);
                }
            });

            return jSchema;
        }

        public static async Task<JObject> ReadJObjectAsync(Stream stream)
        {
            JObject jObject = null;

            await Task.Factory.StartNew(() =>
            {

                using (var jsonReader = new JsonTextReader(new StreamReader(stream)))
                {
                    jObject = JObject.Load(jsonReader);
                }
            });

            return jObject;
        }

        public static async Task<JSchema> ReadJSchemaAsync(Stream stream)
        {
            JSchema jSchema = null;

            await Task.Factory.StartNew(() =>
            {
                using (var jsonReader = new JsonTextReader(new StreamReader(stream)))
                {
                    jSchema = JSchema.Load(jsonReader);
                }
            });

            return jSchema;
        }

        public static async Task<JSchema> JObjectToJSchema(JObject jsonObject)
        {
            JSchema languageJsonValidatingSchema = null;

            await Task.Factory.StartNew(() =>
            {
                // Maybe there is another way without conversion to string
                languageJsonValidatingSchema = JSchema.Parse(jsonObject.ToString());
            });

            return languageJsonValidatingSchema;
        }
    }

}
